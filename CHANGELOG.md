# Changelog for jgxvx/data-types

## Changelog for 6.0.0

### Added
* Support for PHP 8.3 and 8.4

### Removed
* Support for PHP 8.1 and 8.2
* Class `AbstractEnum` (deprecated in v4.0.0)

## Changelog for 5.0.0
* Adding support for PHP 8.2
* Upgrading to PHPUnit 10.3

## Changelog for 4.0.0
* Increasing minimum PHP version to `^8.1`
* Deprecating `AbstractEnum` in favour of PHP 8.1's native enumerations

## Changelog for 3.0.0
* Narrowing allowed types for `AbstractEnum` values down to `int`, `float`, `string` and `bool`.

## Changelog for 2.0.0
* Increased minimum PHP version to `^8.0`
* Upgraded PHP-CS-Fixer dependency to `^3.2`

## Changelog for 1.0.0
* First major release
