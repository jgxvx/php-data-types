tag := "8.3"
registry := "registry.gitlab.com"
project := "jgxvx/php-data-types"
image := "php"

build-test-image tag:
	podman build --build-arg PHP_VERSION={{ tag }} -t {{ registry }}/{{ project }}/{{ image }}:{{ tag }} -f ./devops/ci/Containerfile .

push-test-image tag:
	podman push {{ registry }}/{{ project }}/{{ image }}:{{ tag }}
