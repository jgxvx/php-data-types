# jgxvx/data-types

A small collection of convenience classes for dealing with different datatypes in PHP.

## `Enum\AbstractEnum`

An abstract class to act as an Enum of a collection of related values.

## `StringType\Search`

Provides the ability to search for substrings within strings.
Mostly obsolete since PHP 8.0 introduced `str_starts_with`, `str_ends_with` and `str_contains`.

## `StringType\Transformer`

Provides the ability to transform strings from `snake_case` or `kebab-case` to `camelCase` and vice versa.
