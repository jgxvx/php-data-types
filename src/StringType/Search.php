<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

class Search
{
    public static function beginsWith(string $haystack, string $needle, bool $caseInsensitive = false): bool
    {
        if ($caseInsensitive) {
            return \stripos($haystack, $needle) === 0;
        }

        return \str_starts_with($haystack, $needle);
    }

    public static function endsWith(string $haystack, string $needle, bool $caseInsensitive = false): bool
    {
        if ($caseInsensitive) {
            $pos = \strripos($haystack, $needle);

            return $pos === \strlen($haystack) - \strlen($needle);
        }

        return \str_ends_with($haystack, $needle);
    }

    public static function contains(string $haystack, string $needle, bool $caseInsensitive = false): bool
    {
        if ($caseInsensitive) {
            return \stripos($haystack, $needle) !== false;
        }

        return \str_contains($haystack, $needle);
    }
}
