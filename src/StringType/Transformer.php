<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

class Transformer
{
    public static function toCamelCase(string $snakeCase, bool $ucfirst = false): string
    {
        $camelCase = \ucwords(\strtolower($snakeCase), '_-');
        $camelCase = \str_replace(['_', '-'], '', $camelCase);

        if ($ucfirst) {
            return \ucfirst($camelCase);
        }

        return \lcfirst($camelCase);
    }

    public static function toSnakeCase(string $camelCase, string $delimiter = '_', bool $toUpperCase = false): string
    {
        $snakeCase = \strtolower(\preg_replace('/(?<!^)[A-Z]/', '_$0', $camelCase));

        if ($delimiter !== '_') {
            $snakeCase = \str_replace('_', $delimiter, $snakeCase);
        }

        if ($toUpperCase) {
            return \strtoupper($snakeCase);
        }

        return \strtolower($snakeCase);
    }
}
