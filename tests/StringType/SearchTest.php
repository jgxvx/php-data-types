<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

#[CoversClass(Search::class)]
final class SearchTest extends TestCase
{
    #[Test]
    #[DataProviderExternal(SearchTestDataProvider::class, 'stringBeginsWithData')]
    public function stringBeginsWithNeedle(string $haystack, string $needle, bool $caseInsensitive, bool $expected): void
    {
        self::assertSame($expected, Search::beginsWith($haystack, $needle, $caseInsensitive));
    }

    #[Test]
    #[DataProviderExternal(SearchTestDataProvider::class, 'stringEndsWithData')]
    public function stringEndsWithNeedle(string $haystack, string $needle, bool $caseInsensitive, bool $expected): void
    {
        self::assertSame($expected, Search::endsWith($haystack, $needle, $caseInsensitive));
    }

    #[Test]
    #[DataProviderExternal(SearchTestDataProvider::class, 'containedStringIsFoundData')]
    public function containedStringIsFound(string $haystack, string $needle, bool $caseInsensitive, bool $expected): void
    {
        self::assertSame($expected, Search::contains($haystack, $needle, $caseInsensitive));
    }
}
