<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

final class SearchTestDataProvider
{
    public static function stringBeginsWithData(): array
    {
        return [
            ['Foo', 'F', false, true],
            ['Foo', 'f', false, false],
            ['Foo', 'f', true, true],
            ['Foo', 'fOo', true, true],
            ['Foo', 'fOo', false, false],
            ['Foo', 'Foo', false, true],
        ];
    }

    public static function stringEndsWithData(): array
    {
        return [
            ['Foo', 'o', false, true],
            ['Foo', 'O', false, false],
            ['Foo', 'O', true, true],
            ['Foo', 'Foo', true, true],
            ['Foo', 'Fo', true, false],
            ['Foo', 'Fo', false, false],
        ];
    }

    public static function containedStringIsFoundData(): array
    {
        return [
            ['Foo', 'oo', false, true],
            ['Foo', 'oo', true, true],
            ['Foo', 'OO', false, false],
            ['Foo', 'OO', true, true],
            ['Hello, world', ',', false, true],
            ['Hello, world', ',', true, true],
        ];
    }
}
