<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

#[CoversClass(Transformer::class)]
class TransformerTest extends TestCase
{
    #[Test]
    #[DataProviderExternal(TransformerTestDataProvider::class, 'transformedToCamelCaseData')]
    public function snakeCaseIsTransformedToCamelCase(string $snakeCase, bool $ucfirst, string $expected): void
    {
        self::assertSame($expected, Transformer::toCamelCase($snakeCase, $ucfirst));
    }

    #[Test]
    #[DataProviderExternal(TransformerTestDataProvider::class, 'transformedToSnakeCaseData')]
    public function camelCaseIsTransformedToSnakeCase(string $camelCase, string $delimiter, bool $toUpperCase, string $expected): void
    {
        self::assertSame($expected, Transformer::toSnakeCase($camelCase, $delimiter, $toUpperCase));
    }
}
