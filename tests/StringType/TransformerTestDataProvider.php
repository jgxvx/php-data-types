<?php
declare(strict_types=1);
namespace Jgxvx\DataTypes\StringType;

final class TransformerTestDataProvider
{
    public static function transformedToCamelCaseData(): array
    {
        return [
            ['foo_bar', false, 'fooBar'],
            ['foo_bar', true, 'FooBar'],
            ['foo-bar-baz', true, 'FooBarBaz'],
            ['foo-bar-baz', false, 'fooBarBaz'],
        ];
    }

    public static function transformedToSnakeCaseData(): array
    {
        return [
            ['fooBar', '_', false, 'foo_bar'],
            ['fooBar', '_', true, 'FOO_BAR'],
            ['FooBar', '-', true, 'FOO-BAR'],
            ['FooBar', '-', false, 'foo-bar'],
            ['fooBarBaz', '_', false, 'foo_bar_baz'],
            ['fooBarBaz', '_', true, 'FOO_BAR_BAZ'],
        ];
    }
}
